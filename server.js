var express = require('express');
var app = express();
var path = require("path");
var logger = require("morgan");
var bodyParser = require("body-parser");
var http = require('http').Server(app);
var path = require('path');

//set the view engine to ejs
app.set('view engine', 'ejs');

// set up the http request logger
//app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));

// Initialize appication with route / (that means root of the application)
app.use(express.static(path.join(__dirname+'/views/partials/')));
app.use(express.static(__dirname + '/views/partials'));
app.set("views", path.resolve(__dirname, "views")); // path to views
//soruce: https://scotch.io/tutorials/use-ejs-to-template-your-node-application
//use res.render to load up an ejs view file
//index page
app.get('/', function(req, res) {
    res.render('pages/index');
});

//aboutMe page
app.get('/aboutMe', function(req, res){
    res.render('pages/aboutMe');
});

//Contacts page
app.get('/Contacts', function(req, res){
    res.render('pages/Contacts');
});

//guestbook page
app.get('/guestbook', function(req, res){
    res.render('pages/guestbook');
});
app.get('/new-entry', function(req, res){
    res.render('pages/new-entry');
});

// create an array to manage guestbook entries
var entries = [];
app.locals.entries = entries;

// http POST (INSERT)
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("/guestbook");
});



app.listen(8081);
console.log("Server runs at 8081");