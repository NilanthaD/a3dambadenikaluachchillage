# A03 - USE of EXPRESS, EJS and NODE to develop an applicaiton and guest book

Use EJS to template my NODE application

Use Express framework for node.js

Use Express framework to create a guest book application

## How to use

Open a command window in your your content folder.

Run npm install to install all the dependencies in the package.json file.

Run node server.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> nodemon server.js
```

Point your browser to `http://localhost:8081`.

## Reference

Based on http://javabeginnerstutorial.com/javascript-2/create-simple-chat-application-using-node-js-express-js-socket-io/
and https://bitbucket.org/professorcase/w06

https://scotch.io/tutorials/use-ejs-to-template-your-node-application

images: https://pixabay.com/en/cloth-blue-modern-background-art-569222*/